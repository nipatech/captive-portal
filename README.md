# NIPA WIFI SERVICE Captive Portal
A customized captive portal based on demo.html provided by OMADA Controller for Linux v4.1.5

## Get Started
When editing [tplink-captive-portal.html](tplink-captive-portal.html), use inline css and base64 encoded images. Never include external assets as it wont load unless its domain is added on Pre-Authentication Access List. 

## Built With
* [PureCSS](https://purecss.io/) - The UI framework used

## Authors

* **Christian Nipa** - *Initial work* - [NipaTech](https://github.com/nipatech)
